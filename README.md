Role Name
=========
Конфигурирование systemd timesyncd для синхронизации точного времени на VM.

Role Variables
--------------
Необходимо задать переменную ntp-servers: в зависимости от георграфического расположения VM. 

Example Playbook
----------------

	- hosts: all
	  gather_facts: yes
	  become: yes
	  vars:
		  ntps_servers: ip.addr.ntp.serv
	  roles:
	    - ntp_deploy

License
-------
MIT

Author Information
------------------
Konstantin Khazov <khazovkv@gmail.com>

